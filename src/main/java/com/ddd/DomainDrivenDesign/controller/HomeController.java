package com.ddd.DomainDrivenDesign.controller;

import com.ddd.DomainDrivenDesign.model.User;
import com.ddd.DomainDrivenDesign.service.UserRegistrationDTOService;
import com.ddd.DomainDrivenDesign.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {

    private final UserRegistrationDTOService userRegistrationDTOService;
    private final Validator userValidator;

    @Autowired
    public HomeController(UserRegistrationDTOService userRegistrationDTOService, @Qualifier("userValidator") Validator userValidator) {
        this.userRegistrationDTOService = userRegistrationDTOService;
        this.userValidator = userValidator;
    }

    @InitBinder("user")
    public void initBinding(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }

    @GetMapping
    public ModelAndView getIndex() {
        ModelAndView mnv = new ModelAndView("index");
        mnv.addObject("user", new User());
        return mnv;
    }

    @PostMapping(value = "/registerUser")
    public String registerUser(@ModelAttribute("user") @Validated User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "index";
        }

        return "redirect:/userData";

    }
}