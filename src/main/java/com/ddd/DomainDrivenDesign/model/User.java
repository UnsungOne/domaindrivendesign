package com.ddd.DomainDrivenDesign.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@ToString
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private boolean isRemembered;

    public User(String name, String surname, String email, String password, boolean isRemembered) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.isRemembered = isRemembered;
    }
}