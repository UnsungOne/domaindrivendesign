package com.ddd.DomainDrivenDesign.repository;


import com.ddd.DomainDrivenDesign.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginDTORepository extends JpaRepository<User, Long> {
}
