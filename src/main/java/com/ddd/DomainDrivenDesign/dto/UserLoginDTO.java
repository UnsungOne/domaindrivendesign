package com.ddd.DomainDrivenDesign.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLoginDTO {

    private Long id;
    private String email;
    private String password;

}
