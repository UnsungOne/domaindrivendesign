package com.ddd.DomainDrivenDesign.validator;

import com.ddd.DomainDrivenDesign.model.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.servlet.http.HttpServletRequest;

@Component("userValidator")
public class UserValidator implements Validator {

    private RegexpValidator regexpValidator;
    private HttpServletRequest httpServletRequest;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        ValidationUtils.rejectIfEmpty(errors, "name", "user.validator.field.notEmpty");
        ValidationUtils.rejectIfEmpty(errors, "surname", "user.validator.field.notEmpty");
        ValidationUtils.rejectIfEmpty(errors, "email", "user.validator.field.notEmpty");
        ValidationUtils.rejectIfEmpty(errors, "password", "user.validator.field.notEmpty");
        ValidationUtils.rejectIfEmpty(errors, "confirm-password", "user.validator.field.notEmpty");
        validatePasswordMatch(user, httpServletRequest, errors);
    }


    private static void validatePasswordMatch(Object o, HttpServletRequest request, Errors errors){
        User user = (User) o;
        if (!user.getPassword().equals(request.getParameter("confirm-password"))){
            errors.rejectValue("password", "user.validator.field.passwordNotMatch");
        }
    }
}
